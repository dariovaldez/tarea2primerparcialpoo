package facci.pm.ta2.poo.pra1;

import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import facci.pm.ta2.poo.datalevel.DataException;
import facci.pm.ta2.poo.datalevel.DataObject;
import facci.pm.ta2.poo.datalevel.DataQuery;
import facci.pm.ta2.poo.datalevel.GetCallback;

public class DetailActivity extends AppCompatActivity {

    TextView tv_price, tv_description, tv_nombre;
    ImageView iv_imagen;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("PR1 :: Detail");

        tv_nombre = (TextView) findViewById(R.id.name);
        tv_price = (TextView) findViewById((R.id.price));
        tv_description = (TextView) findViewById(R.id.description);
        iv_imagen = (ImageView) findViewById(R.id.thumbnail);



        // INICIO - CODE6

        String parametro = getIntent().getExtras().getString("Dato");
        final DataQuery query = DataQuery.get("item");
        query.getInBackground(parametro, new GetCallback<DataObject>() {
            @Override
            public void done(DataObject object, DataException e) {
                if (e==null){
                    String price = (String) object.get("price");
                    String description = (String) object.get("description");
                    String name = (String) object.get("name");
                    Bitmap image = (Bitmap) object.get("image");

                    tv_price.setText(price + "$");
                    tv_price.setTextColor(getColor(R.color.precioRojo));
                    tv_description.setText(description);
                    tv_nombre.setText(name);
                    iv_imagen.setImageBitmap(image);
                }
            }
        });



        // FIN - CODE6

    }

}
